import React from "react";
import alexandr from "./img/alexandr.jpg";
import jons from "./img/jonas.jpg";
import veras from "./img/veras.jpg";

export default class App extends React.Component{
render(){
  return(
    <Counter/>
  )
}
}
class Counter extends React.Component{
  render(){
    return(
      <section style={{
        background: "#373550",
        display:"flex",
        alignItems:"center",
        justifyContent:"center",
        gap:"20px",
        minHeight:"100vh",
        padding:"0px 20px"
      }}>
        <div style={
          {
            flex:"1",
            display:"flex",
            flexDirection:"column",
            justifyContent:"center",
            alignItems:"center",
            background:"rgb(0, 0, 0,0.1)",
            borderRadius:"20px",
            padding:"30px"

          }
        }>
          <div style={{ 
            width:"220px",
             height:"220px",
             borderRadius:"50%", 
             background: "linear-gradient(45deg,#ADFF2F 0%,#0000CD 100%)",
             display:"flex",
             alignItems:"center",
             marginTop:"-100px",
             justifyContent:"center" }}>
             <img src={alexandr} alt="alexandr" style={
          {
            width:"200px",
            height:"200px",
            borderRadius:"100%",
            marginTop:"0px"
           
          }
          }/>
          </div>
         
          <h1 style={
            {
              color:"white"
            }
          }>Thomas Scheer</h1>
          <p style={{
            color:"grey",
            marginTop:"-10px"
          }}>Digital Marketer</p>
          <p style={{
            color:"white"
          }}>Aboute me</p>
          <p style={
            {
              color:"white",
              textAlign:"center",
              width:"100%",
              paddingBottom:"20px",
              borderBottom:"2px solid grey"
            }
          }>I like jazz music and bacon.Learning new <br/> things is one of my obsessions</p>
          <button style={
            {
              borderRadius:"50px",
              padding:"10px 20px",
              border:"none",
              background:"rgba(132, 128, 128, 0.1)",
              color:"white",
              cursor:"pointer"
            }
          }>ADD USER</button>
        </div>
        <div style={
          {
            flex: "1",
            display: "flex",
            flexDirection: "column",
            justifyContent: "center",
            alignItems: "center",
            background: "rgb(0, 0, 0,0.1)",
            borderRadius: "20px",
            padding: "30px"

          }
        }>
          <div style={{
            width: "220px",
            height: "220px",
            borderRadius: "50%",
            background: "linear-gradient(180deg,#F07427 0%,#F4CA16 100%)",
            display: "flex",
            alignItems: "center",
            marginTop: "-100px",
            justifyContent: "center"
          }}>
            <img src={veras} alt="alexandr" style={
              {
                width: "200px",
                height: "200px",
                borderRadius: "100%",
                marginTop: "0px"

              }
            } />
          </div>

          <h1 style={
            {
              color: "white"
            }
          }>Amanda Green</h1>
          <p style={{
            color: "grey",
            marginTop: "-10px"
          }}>UX Deigner</p>
          <p style={{
            color: "white"
          }}>Aboute me</p>
          <p style={
            {
              color: "white",
              textAlign: "center",
              width: "100%",
              paddingBottom: "20px",
              borderBottom: "2px solid grey"
            }
          }>Passionate th nerd and Welong web <br/> junkie. I love sleeping.</p>
          <button style={
            {
              borderRadius: "50px",
              padding: "10px 20px",
              border: "none",
              background: "rgba(132, 128, 128, 0.1)",
              color: "white",
              cursor:"pointer"
            }
          }>ADD USER</button>
        </div>
        <div style={
          {
            flex: "1",
            display: "flex",
            flexDirection: "column",
            justifyContent: "center",
            alignItems: "center",
            background: "rgb(0, 0, 0,0.1)",
            borderRadius: "20px",
            padding: "30px"

          }
        }>
          <div style={{
            width: "220px",
            height: "220px",
            borderRadius: "50%",
            background: "linear-gradient(-150deg,#F4CA16 10%,#9457EB 100%)",
            display: "flex",
            alignItems: "center",
            marginTop: "-100px",
            justifyContent: "center"
          }}>
            <img src={jons} alt="alexandr" style={
              {
                width: "200px",
                height: "200px",
                borderRadius: "100%",
                marginTop: "0px"

              }
            } />
          </div>

          <h1 style={
            {
              color: "white"
            }
          }>Shane Wright</h1>
          <p style={{
            color: "grey",
            marginTop: "-10px"
          }}>IT Manager</p>
          <p style={{
            color: "white"
          }}>Aboute me</p>
          <p style={
            {
              color: "white",
              textAlign: "center",
              width: "100%",
              paddingBottom: "20px",
              borderBottom: "2px solid grey"
            }
          }>Avaid consumer of Chinese food and <br /> old movies.Old soul</p>
          <button style={
            {
              borderRadius: "50px",
              padding: "10px 20px",
              border: "none",
              background: "rgba(132, 128, 128, 0.1)",
              color: "white",
              cursor:"pointer"
            }
          }>ADD USER</button>
        </div>
      </section>
    )
  }
}
